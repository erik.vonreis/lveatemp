import nds2
import gpstime
import scipy.signal as signal
import numpy as np
from alarm import AlarmGroup, Alarm, gps_to_timestamp, merge_alarm_group_histories, MAX_ALARM_HISTORY
import logging
import pcaspy
from math import isnan

# LVEA temperature sensors.
# A dictionary of dictionaries. Top dictionary is keyed on zone,
# entry is dictionary of sensors in that zone.
# sub-dictionary is keyed on sensor name, entry is sensor outlier count:
#    0 = no outliers
#    n = incremental count of outliers
# Sensor name is H0:FMC-CS_LVEA_ZONE{zone}_{sensor}_DEGC
#
LVEA_ZONES = {"1A": {"C": 0,
                     "D": 0,
                     "E": 0,
                     "F": 0},
              "1B": {"A": 0,
                     "B": 0},
              "2A": {"A": 0,
                     "C": 0,
                     "D": 0},
              "2B": {"B": 0,
                     "E": 0,
                     "F": 0,
                     "G": 0},
              "3A": {"F": 0,
                     "G": 0,
                     "H": 0},
              "3B": {"A": 0,
                     "B": 0,
                     "C": 0,
                     "D": 0},
              "4": {"A": 0,
                    "B": 0,
                    "C": 0,
                    "E": 0,
                    "F": 0,
                    "G": 0,
                    "H": 0},
              "5": {"A": 0,
                    "B": 0,
                    "C": 0,
                    "D": 0,
                    "E": 0,
                    "F": 0,
                    "G": 0,
                    "H": 0,
                    "I": 0,
                    "J": 0},
              }


# 60 seconds per minute.  Using minute trends.
PERIOD = 60

# a value the temperature likes to jump to when it's in error
RAIL_HIGH = 40

# another value the temperature likes to jump to in error
RAIL_LOW = 0

LIMIT_HIGH = 105
LIMIT_LOW = -45


# default values for too high and too low temperature

DEFAULT_HIGH_DEGC = 30
DEFAULT_LOW_DEGC = 10

# maximum allowable filtered temperature change per minute
# before an alarm is triggered
rate_limit_degc_per_min = 0.001

# how far in the past to look bac
# need to delay to give NDS time to collect data
DELAY_S = 300


class FilteredData(object):
    """Represents filtered output, including filter, and filter history"""

    def __init__(self, filt):
        self.filter = filt
        self.data = np.array([])

        # filter tap history
        # needs to later be multiplied by data 0
        self.zi = signal.lfilter_zi(*filt[:2])

    def add_data(self, data):
        if len(data) < 1:
            return
        if len(self.data) < 1:
            self.zi *= data[0]
        new_data, self.zi = signal.lfilter(*self.filter[:2], x=data, zi=self.zi)
        self.data = np.concatenate((self.data, new_data))

    def trim(self, n):
        """
        Trim off the first n values
        """
        if n > len(self.data):
            raise Exception(f"Trying to trim {n} values from an array {len(self.data)} long")
        self.data = self.data[n:]


MAX_ARRAY_SIZE = 4096


class Zone(object):
    def __init__(self, name, sensors):
        self.name = name
        self.sensors = [Sensor(self, sensor) for sensor in sensors]

        # weighted average based off immediate quality (immediately excludes bad channels)
        self.unfiltered_avg_data = np.array([])

        # weighted average based off filtered quality (quality for a channel degrades over time.
        self.filtered_avg_data = np.array([])

        # derivative of filtered_avg_data
        self.deriv_data = FilteredData(create_filtered_deriv())

        # average the quality
        self.quality = np.array([])
        self.filtered_quality = np.array([])

        # set some default alarm levels
        self.too_high_degc = DEFAULT_HIGH_DEGC
        self.too_low_degc = DEFAULT_LOW_DEGC
        self.max_rate_degc_per_min = rate_limit_degc_per_min

        # index to which we've already checked for error
        self.checked_ptr = 0

        # create some alarms
        self.fault_alarms = [Alarm(f"{name}_{sensor.name}_FAULT", f"LVEA temp. {self.name}.{sensor.name} fault",
                                   lambda: sensor.fault(), self.quality_pv, pcaspy.Severity.MINOR_ALARM)
                             for sensor in self.sensors]

        self.high_alarm = Alarm(f"{name}_HIGH", f"LVEA zone {name} temp. high",
                                lambda: self.check_high(), self.temp_pv)
        self.low_alarm = Alarm(f"{name}_LOW", f"LVEA zone {name} temp. low",
                               lambda: self.check_low(), self.temp_pv)
        self.rise_alarm = Alarm(f"{name}_RISE", f"LVEA zone {name} temp. rising fast",
                                lambda: self.check_rise(), self.rate_pv)
        self.fall_alarm = Alarm(f"{name}_FALL", f"LVEA zone {name} temp. falling fast",
                                lambda: self.check_fall(), self.rate_pv)

    def check_high(self):
        assert self.checked_ptr < len(self.filtered_avg_data)
        return self.filtered_avg_data.data[self.checked_ptr] >= self.too_high_degc

    def check_low(self):
        assert self.checked_ptr < len(self.filtered_avg_data)
        return self.filtered_avg_data.data[self.checked_ptr] <= self.too_low_degc

    def check_rise(self):
        assert self.checked_ptr < len(self.filtered_avg_data)
        return self.deriv_data.data[self.checked_ptr] >= self.max_rate_degc_per_min

    def check_fall(self):
        assert self.checked_ptr < len(self.filtered_avg_data)
        return self.deriv_data.data[self.checked_ptr] <= -self.max_rate_degc_per_min

    def check_alarms(self):
        """
        """
        while self.checked_ptr < len(self.unfiltered_avg_data):
            check_time_s = self.sensors[0].time_from_index(self.checked_ptr)
            for alarm in self.fault_alarms:
                alarm.check(check_time_s)
            self.high_alarm.check(check_time_s)
            self.low_alarm.check(check_time_s)
            self.rise_alarm.check(check_time_s)
            self.fall_alarm.check(check_time_s)

            # print(f"Checked {self.checked_ptr*PERIOD + self.sensors[0].start_time_s}")
            self.checked_ptr += 1

    def unfiltered_average(self, old_len, new_len):
        """Get an average temperature that uses the unfiltered quality of each sensor as a weight"""
        num_pts = new_len - old_len

        new_sum = np.zeros(num_pts)
        total_weight = np.zeros(num_pts)
        for sensor in self.sensors:
            # prevent divide by zero
            quality_minimums = np.zeros(new_len - old_len) + 0.001
            quality = np.maximum(quality_minimums, sensor.quality[old_len:])
            total_weight += quality
            new_sum += quality * sensor.data[old_len:]

        new_avg = new_sum / total_weight
        self.unfiltered_avg_data = np.concatenate((self.unfiltered_avg_data, new_avg))

    def filtered_average(self, old_len, new_len):
        """Get an average temperature that uses the filtered quality of each sensor as a weight"""
        num_pts = new_len - old_len
        new_sum = np.zeros(num_pts)
        total_weight = np.zeros(num_pts)
        for sensor in self.sensors:
            # prevent divide by zero
            quality_minimums = np.zeros(new_len - old_len) + 0.001
            quality = np.maximum(quality_minimums, sensor.filtered_quality.data[old_len:])
            total_weight += quality
            new_sum += quality * sensor.data[old_len:]

        new_avg = new_sum / total_weight
        self.filtered_avg_data = np.concatenate((self.filtered_avg_data, new_avg))

    def quality_average(self, old_len, new_len):
        """
        Get averages for both filtered and unfiltered quality
        """
        num_pts = new_len - old_len
        unf_new_sum = np.zeros(num_pts)
        filt_new_sum = np.zeros(num_pts)

        for sensor in self.sensors:
            unf_new_sum += sensor.quality[old_len:]
            filt_new_sum += sensor.filtered_quality.data[old_len:]
        unf_new_avg = unf_new_sum / len(self.sensors)
        filt_new_avg = filt_new_sum / len(self.sensors)

        self.quality = np.concatenate((self.quality, unf_new_avg))
        self.filtered_quality = np.concatenate((self.filtered_quality, filt_new_avg))

    def averages(self):
        """
        Process averages for new data
        """
        new_len = len(self.sensors[0].data)
        old_len = len(self.unfiltered_avg_data)
        if new_len > old_len:
            self.unfiltered_average(old_len, new_len)
            self.filtered_average(old_len, new_len)
            self.quality_average(old_len, new_len)
        else:
            # no new data
            pass

    def filter(self):
        """
        Apply any filters after everything else is run
        """
        new_len = len(self.filtered_avg_data)
        old_len = len(self.deriv_data.data)

        if new_len > old_len:
            self.deriv_data.add_data(self.filtered_avg_data[old_len:])
        else:
            # no data to filter
            pass

    def tick(self):
        """
        Update zone data to take in recent sensor data
        """
        for sensor in self.sensors:
            sensor.process()
            sensor.filter()

        self.averages()
        self.filter()
        if len(self.deriv_data.data) > MAX_ARRAY_SIZE:
            self.trim(len(self.deriv_data.data) - MAX_ARRAY_SIZE)

    def trim(self, n):
        """trim n from front of all arrays"""
        for sensor in self.sensors:
            sensor.trim(n)

        self.unfiltered_avg_data = self.unfiltered_avg_data[n:]
        self.filtered_avg_data = self.filtered_avg_data[n:]
        self.deriv_data.trim(n)
        self.quality = self.quality[n:]
        self.filtered_quality = self.filtered_quality[n:]

        if n >= self.checked_ptr:
            self.checked_ptr = 0
        else:
            self.checked_ptr -= n

        self.check_sizes()

    def check_sizes(self):
        """
        check all arrays are the same size
        """
        assert len(self.unfiltered_avg_data) == len(self.filtered_avg_data)
        assert len(self.unfiltered_avg_data) == len(self.deriv_data.data)
        assert len(self.deriv_data.data) == len(self.quality)
        assert len(self.filtered_quality) == len(self.quality)
        for sensor in self.sensors:
            sensor.check_sizes(len(self.quality))

    def reset_checked(self):
        """Reset checked to end of data"""
        self.checked_ptr = len(self.unfiltered_avg_data)

    @property
    def temp_pv(self):
        return f"ZONE{self.name}_DEGC"

    @property
    def rate_pv(self):
        return f"ZONE{self.name}_DEGC_PER_MIN"

    @property
    def quality_pv(self):
        return f"ZONE{self.name}_QUALITY"

    @property
    def hi_alarm_pv(self):
        return f"ZONE{self.name}_HIALARM_DEGC"

    @property
    def lo_alarm_pv(self):
        return f"ZONE{self.name}_LOALARM_DEGC"

    @property
    def rate_alarm_pv(self):
        return f"ZONE{self.name}_RATEALARM_DEGC_PER_MIN"


class Sensor(object):
    """Represents a single LVEA temperature channel"""

    def __init__(self, zone, name):
        self.zone = zone
        self.name = name

        self.start_time_s = 0

        self.raw_mean = np.array([])
        self.raw_max = np.array([])
        self.raw_min = np.array([])
        self.raw_sdev = np.array([])
        self.process_ptr = 0
        self.hold_value = -120

        # data arrays
        self.data = np.array([])  # bad values glossed over
        self.quality = np.array([])
        self.filtered_quality = FilteredData(create_filter())

        """process state can be 'error', 'railed_low', 'railed_high, 'good'
        If in error, any limited value will be replaced.  If railed_low, any value below RAIL_HIGH will be used.
        If in good state, any value at the rails will either cause a switch to error state, or to a railed state,
        depending on how big the jump was to get there.  If jump > 1 deg C, go to error state.
        """
        self.process_state = "error"

        # must be at least 1
        self.settling_time_min = 5

        # time remaining before a value is considered good
        self.settling_remaining_min = 0

    def trim(self, n):
        """Trim n values from the front of every array"""
        self.start_time_s += PERIOD * n

        self.raw_mean = self.raw_mean[n:]
        self.raw_max = self.raw_max[n:]
        self.raw_min = self.raw_min[n:]
        self.raw_sdev = self.raw_sdev[n:]

        self.data = self.data[n:]
        self.quality = self.quality[n:]
        self.filtered_quality.trim(n)
        self.process_ptr -= n

    def check_sizes(self, n):
        """
        Check that all arrays are of size n
        """
        assert len(self.raw_mean) == n
        assert len(self.raw_max) == n
        assert len(self.raw_min) == n
        assert len(self.raw_sdev) == n

        assert len(self.data) == n
        assert len(self.quality) == n
        assert len(self.filtered_quality.data) == n

    def filter(self):
        """Filter any new data

        Assumes self.quality and self.data are processed
        """
        l_q = len(self.quality)
        l_fq = len(self.filtered_quality.data)

        if l_fq >= l_q:
            # nothing to filter
            pass
        else:
            self.filtered_quality.add_data(self.quality[l_fq:])

    def add_data(self, start_time_s, mean, sdev, data_max, data_min):
        assert len(mean) == len(sdev)
        assert len(mean) == len(data_max)
        assert len(mean) == len(data_min)
        if len(self.raw_mean) == 0:
            self.start_time_s = start_time_s
            self.raw_mean = np.array(mean)
            self.raw_min = np.array(data_min)
            self.raw_max = np.array(data_max)
            self.raw_sdev = np.array(sdev)
        else:
            if self.start_time_s <= start_time_s < self.end_time_s:
                """Overlap.  Make sure values are the same."""
                overlap_pts = (self.end_time_s - start_time_s) / PERIOD
                overlap_start = -overlap_pts
                overlap_pts = min(overlap_pts, len(mean))
                if (mean[:overlap_pts] != self.raw_mean[overlap_start:(overlap_start + overlap_pts)]
                        or data_min[:overlap_pts] != self.raw_min[overlap_start:(overlap_start + overlap_pts)]
                        or data_max[:overlap_pts] != self.raw_max[overlap_start:(overlap_start + overlap_pts)]
                        or sdev[:overlap_pts] != self.raw_sdev[overlap_start:(overlap_start + overlap_pts)]):
                    raise Exception(f"An overlapping segment was added to {self.name} starting at {start_time_s} "
                                    " but did not match previously stored data")
                if overlap_pts < len(mean):
                    return self.add_data(self.end_time_s, mean[overlap_pts:], sdev[overlap_pts:],
                                         data_max[overlap_pts:], data_min[overlap_pts:])
                else:
                    return
            elif start_time_s < self.start_time_s:
                raise Exception("cannot add data to beginning of trace")
            elif start_time_s > self.end_time_s:
                raise Exception("cannot create a gap in data")
            else:
                self.raw_mean = np.concatenate((self.raw_mean, mean))
                self.raw_sdev = np.concatenate((self.raw_sdev, sdev))
                self.raw_min = np.concatenate((self.raw_min, data_min))
                self.raw_max = np.concatenate((self.raw_max, data_max))

    def process(self):
        global RAIL_LOW, RAIL_HIGH, LIMIT_LOW, LIMIT_HIGH
        while self.process_ptr < len(self.raw_mean):
            mean = self.raw_mean[self.process_ptr]
            min_pt = self.raw_min[self.process_ptr]
            max_pt = self.raw_max[self.process_ptr]

            # handle state transitions
            if self.process_state == 'error':
                # reset time so that we keep holding the last good value
                if min_pt <= RAIL_LOW or max_pt >= RAIL_HIGH:
                    pass  # remain in error state
                elif isnan(min_pt) or isnan(max_pt) or isnan(mean):
                    pass # any NaN means error
                else:
                    self.process_state = 'good'
            elif self.process_state == 'good':
                if isnan(min_pt) or isnan(max_pt) or isnan(mean):
                    self.process_state = 'error'
                elif min_pt <= RAIL_LOW:
                    if np.fabs(min_pt - self.hold_value) <= 1.0:
                        self.process_state = 'railed_low'
                    else:
                        self.process_state = 'error'
                elif max_pt >= RAIL_HIGH:
                    if np.fabs(max_pt - self.hold_value) <= 1.0:
                        self.process_state = 'railed_high'
                    else:
                        self.process_state = 'error'
            elif self.process_state == 'rail_low':
                logging.critical(f"{self.zone.name}.{self.name} Railed Low State")
                if isnan(min_pt) or isnan(max_pt) or isnan(mean):
                    self.process_state = 'error'
                elif min_pt <= RAIL_LOW:
                    pass
                elif max_pt >= RAIL_HIGH:
                    self.process_state = 'error'
                else:
                    self.process_state = 'good'
            elif self.process_state == 'rail_high':
                logging.critical(f"{self.zone.name}.{self.name} Railed High State")
                if isnan(min_pt) or isnan(max_pt) or isnan(mean):
                    self.process_state = 'error'
                elif max_pt >= RAIL_HIGH:
                    pass
                elif min_pt <= RAIL_LOW:
                    self.process_state = 'error'
                else:
                    self.process_state = 'good'
            else:
                raise Exception(f"Unknown process state {self.process_state}")

            # if in error state, reset timeoout
            if self.process_state == 'error':
                self.settling_remaining_min = self.settling_time_min

            # sensors sometimes read bad values for a few minutes
            # after coming back online
            if self.settling_remaining_min <= 0:
                quality = 1
                new_value = mean
            else:
                quality = 0
                new_value = self.hold_value
                self.settling_remaining_min -= 1

            # limit the values to avoid crazy results and especially INFs
            # that will destroy IIRs
            new_value = min(new_value, LIMIT_HIGH)
            new_value = max(new_value, LIMIT_LOW)
            # and also NaNs, those destroy IIRs too and also min, max.
            # this should never happen, though, since NaNs on input ought to trigger error state.
            if isnan(new_value):
                new_value = (LIMIT_HIGH + LIMIT_LOW) / 2.0
            self.hold_value = new_value

            self.quality = np.append(self.quality, quality)
            self.data = np.append(self.data, new_value)
            self.process_ptr = len(self.data)

    def fault(self):
        assert self.zone.checked_ptr < len(self.quality)
        return self.quality[self.zone.checked_ptr] <= 0

    def time_from_index(self, index):
        """Determine time from an array index"""
        return self.start_time_s + index * PERIOD

    @property
    def end_time_s(self):
        return self.time_from_index(len(self.raw_mean))

    @property
    def channel_name(self):
        return f"H0:FMC-CS_LVEA_ZONE{self.zone.name}_{self.name}_DEGC"


g_zones = []  # type: list["Zone"]
g_chan_list = []  # type: list[str]


def get_zones():
    global g_zones

    if len(g_zones) == 0:
        g_zones = [Zone(zone, zone_dict.keys()) for zone, zone_dict in LVEA_ZONES.items()]

    return g_zones


def poly_prod(p1, p2):
    """
    Multiply two sets of polynomial coefficients together

    Useful for combining filters
    """

    if len(p1) == 0 or len(p2) == 0:
        return np.array([])

    pp = np.zeros(len(p1) + len(p2) - 1)

    for i1 in range(len(p1)):
        for i2 in range(len(p2)):
            c1 = p1[i1]
            c2 = p2[i2]
            pi = i1 + i2
            pp[pi] += c1 * c2
    return pp


def merge_filters(f1, f2):
    """
    Merge digital filters in a,b transfer function format
    """
    return poly_prod(f1[0], f2[0]), poly_prod(f1[1], f2[1])


def create_filter():
    # return signal.iirdesign(1/4000, 1/2880, 4, 30, fs=1.0, ftype='cheby2')
    # return ([0, 0.001],[1, -0.999], 1.0)

    lp = signal.cheby1(1, 5, 1 / 2160, fs=1)
    # stop_band = signal.iirdesign([1/5000,1/60], [1/1500,1/1400], 10, 30, fs=1.0)

    # f = merge_filters(lp, stop_band)

    # for i in range(len(f[0])):
    #     f[0][i] /= 0.3162277659879649
    # for i in range(1, len(f[1])):
    #     f[1][i] /= 0.3162277659879649
    # f[1][0] *= 0.3162277659879649
    return lp


def deriv_filter():
    return [-1 / 12, 8 / 12, 0, -8 / 12, 1 / 12], [1.0]


def create_filtered_deriv():
    f = create_filter()
    d = deriv_filter()
    return merge_filters(f, d)


def get_chan_list():
    global g_chan_list

    if len(g_chan_list) == 0:
        zones = get_zones()
        sensors = []
        for zone in zones:
            sensors += zone.sensors

        for sensor in sensors:
            chan = sensor.channel_name
            g_chan_list.append(chan + ".mean,m-trend")
            g_chan_list.append(chan + ".mean,m-trend")
            g_chan_list.append(chan + ".min,m-trend")
            g_chan_list.append(chan + ".max,m-trend")

    return g_chan_list


def get_data(start_s, stop_s):
    params = nds2.parameters()
    params.set('ALLOW_DATA_ON_TAPE', '1')
    params.set('GAP_HANDLER', 'STATIC_HANDLER_POS_INF')

    zones = get_zones()

    sensors = []
    for zone in zones:
        sensors += zone.sensors

    chan_list = get_chan_list()
    logging.debug(f"fetching {len(chan_list)} channels across {stop_s - start_s} seconds")
    if len(chan_list) > 0:
        # can't do all channels in a single fetch
        chan_batch = 50
        buffers = []
        while len(buffers) < len(chan_list):
            n = min(len(chan_list)-len(buffers), chan_batch)
            try:
                buffers += nds2.fetch(channels=chan_list[len(buffers):len(buffers) + n],
                                      gps_start=start_s, gps_stop=stop_s, params=params)
            except RuntimeError as e:
                logging.error(f"RuntimeError reading from NDS: {str(e)}")
                return
            except TypeError as e:
                # needed to catch a bug in NDS1
                logging.warning(f"TypeError reading from NDS: {str(e)}")
                return

            logging.debug(f"fetched {len(buffers)} channels")
    else:
        raise Exception("chan list should have something in it")

    buf_index = 0
    mean = []
    for sensor in sensors:
        mean = buffers[buf_index].data
        sdev = buffers[buf_index + 1].data
        data_min = buffers[buf_index + 2].data
        data_max = buffers[buf_index + 3].data

        sensor.add_data(start_s, mean, sdev, data_max, data_min)
        buf_index += 4

    logging.debug(f"added {len(mean)} points of data to {len(sensors)} sensors")


def get_leadin_data(early_leadin_s):
    # go back two minutes

    now = int(gpstime.gpsnow()) - DELAY_S

    now -= early_leadin_s

    # round back to prevoius minute boundary
    now -= now % 60

    # start about 24 days earlier
    then = now - 4 * 518400

    logging.debug(f"fetching lead in data starting at {then}")

    get_data(then, now)


def get_test_leadin_data():
    # go back two minutes

    now = int(gpstime.gpsnow()) - DELAY_S

    # round back to prevoius minute boundary
    now -= now % 60

    now -= 120 * 60

    # start about 24 days earlier
    then = now - 4 * 518400

    logging.debug(f"fetching lead in data starting at {then}")

    get_data(then, now)


def get_more_data(update_one_only=False):
    """
    If enough time has passed, get some more data to be processed.

    :param update_one_only: When True, only read in a single minute's worth of data at most.
    """
    # look back two minutes to let NDS handle some data
    now_s = int(gpstime.gpsnow()) - DELAY_S

    # set to minute boundary
    now_s -= now_s % 60

    # take first sensor as representative of all sensors
    zones = get_zones()
    sensor = zones[0].sensors[0]

    if sensor.end_time_s < now_s:

        if update_one_only and ((now_s - sensor.end_time_s) > 60):
            now_s = sensor.end_time_s + 60

        logging.debug(f"fetching more data starting at {sensor.end_time_s}")
        get_data(sensor.end_time_s, now_s)
        # only get 1 minute at a go
        # get_data(sensor.end_time_s, sensor.end_time_s + 60)
    else:
        # too soon.  no data yet
        pass


def get_test_data():
    """
    Get data from Oct 1 to Nov 7 2022
    """
    get_data(1348642800, 1351843200)


def get_test_data2():
    """
    Get data from Oct 1 to Nov 7 2022
    """
    #get_data(1352534400, 1352707200)
    # get_data(1348642800, 1352707200)
    # get_data(1351264800, 1351270800)

    get_data(1353079440 - 600000, 1353279420)


NUM_HISTORICAL_CHANNELS = 10
NUM_CURRENT_ALARMS = 10


class TemperatureScanner(object):
    def __init__(self):
        self.zones = get_zones()

        fault_alarms = []

        self.zone_alarm_groups = []
        for zone in self.zones:
            fault_alarms += zone.fault_alarms
            self.zone_alarm_groups.append(AlarmGroup(f"{zone.name}Alarms", [zone.high_alarm, zone.low_alarm,
                                                                            zone.rise_alarm, zone.fall_alarm]))

        self.fault_alarm_group = AlarmGroup("faults", fault_alarms)

        self.last_zone_alarm_s = 10000
        self.last_fault_alarm_s = 10000

        self.initializing = True

    def prep(self, early_leadin_seconds=0, check_leadin=False):
        """
        Prepare to run by gathering and processing lead-in data

        :param early_leadin_seconds: shift lean in earlier in time by this many seconds.
        A positive value will leave a gap between the end of leadin and current time.  Useful for testing, or
        possibly if you want to raise alerts on recent data.

        :param check_leadin: When True, leave leadin unchecked so that next tick will raise any alarms
        """
        get_leadin_data(early_leadin_seconds)
        # get_test_leadin_data()
        for zone in self.zones:
            zone.tick()
            if not check_leadin:
                zone.reset_checked()

    def tick(self, update_one_only=False):
        """
        :param update_one_only: When true, only tick over one minute of data max
        """
        get_more_data(update_one_only)
        for zone in self.zones:
            zone.tick()
            zone.check_alarms()

    def run_test(self):
        """
        """
        get_test_data()
        for zone in self.zones:
            zone.tick()
            zone.check_alarms()

    def sort_alarm_groups(self):
        """
        Sort alarm groups by time, but also separate by 1 second any identical times
        """
        self.zone_alarm_groups.sort(key=lambda a: a.last_alarm_s)
        for i in range(1, len(self.zone_alarm_groups)):
            if self.zone_alarm_groups[i].last_alarm_s <= self.zone_alarm_groups[i-1].last_alarm_s:
                self.zone_alarm_groups[i].last_alarm_s += 1

    def add_pvs(self, pvdb):
        for zone in self.zones:
            pvdb[zone.temp_pv] = {'type': 'float', 'prec': 2,
                                  'states': [pcaspy.Severity.NO_ALARM, pcaspy.Severity.MAJOR_ALARM]}
            pvdb[zone.rate_pv] = {'type': 'float', 'prec': 5,
                                  'states': [pcaspy.Severity.NO_ALARM, pcaspy.Severity.MAJOR_ALARM]}
            pvdb[zone.quality_pv] = {'type': 'float', 'prec': 3,
                                     'states': [pcaspy.Severity.NO_ALARM, pcaspy.Severity.MINOR_ALARM]}
            pvdb[zone.hi_alarm_pv] = {'type': 'float', 'prec': 2}
            pvdb[zone.lo_alarm_pv] = {'type': 'float', 'prec': 2}
            pvdb[zone.rate_alarm_pv] = {'type': 'float', 'prec': 5}

            # add historical channels
        for i in range(NUM_HISTORICAL_CHANNELS):
            pvdb[f"ALERT_HISTORY_GPS_TIME_{i}"] = {'type': 'int'}
            pvdb[f"ALERT_HISTORY_TIMESTAMP_{i}"] = {'type': 'string'}
            pvdb[f"ALERT_HISTORY_MESSAGE_{i}"] = {'type': 'string'}
            pvdb[f"FAULT_HISTORY_GPS_TIME_{i}"] = {'type': 'int'}
            pvdb[f"FAULT_HISTORY_TIMESTAMP_{i}"] = {'type': 'string'}
            pvdb[f"FAULT_HISTORY_MESSAGE_{i}"] = {'type': 'string'}

        for i in range(NUM_CURRENT_ALARMS):
            pvdb[f"ALERT_GPS_TIME_{i}"] = {'type': 'int'}
            pvdb[f"ALERT_TIMESTAMP_{i}"] = {'type': 'string'}
            pvdb[f"ALERT_MESSAGE_{i}"] = {'type': 'string'}
            pvdb[f"FAULT_GPS_TIME_{i}"] = {'type': 'int'}
            pvdb[f"FAULT_TIMESTAMP_{i}"] = {'type': 'string'}
            pvdb[f"FAULT_MESSAGE_{i}"] = {'type': 'string'}

    def read_pvs(self, driver):
        for zone in self.zones:
            if self.initializing:
                driver.setParam(zone.hi_alarm_pv, zone.too_high_degc)
                driver.setParam(zone.lo_alarm_pv, zone.too_low_degc)
                driver.setParam(zone.rate_alarm_pv, zone.max_rate_degc_per_min)
                driver.setParam("DUMP_ALERTS", 0)
                driver.setParam("DUMP_FAULTS", 0)
                driver.setParam("DUMP_ALERT_HISTORY", 0)
                driver.setParam("DUMP_FAULT_HISTORY", 0)
            else:
                zone.too_high_degc = driver.getParam(zone.hi_alarm_pv)
                zone.too_low_degc = driver.getParam(zone.lo_alarm_pv)
                zone.max_rate_degc_per_min = driver.getParam(zone.rate_alarm_pv)

                dump_alerts = driver.getParam("DUMP_ALERTS")
                dump_faults = driver.getParam("DUMP_FAULTS")
                dump_alert_history = driver.getParam("DUMP_ALERT_HISTORY")
                dump_fault_history = driver.getParam("DUMP_FAULT_HISTORY")

                if dump_alerts != 0:
                    driver.setParam("DUMP_ALERTS", 0)
                    self.dump_alerts()
                if dump_faults != 0:
                    driver.setParam("DUMP_FAULTS", 0)
                    self.dump_faults()
                if dump_alert_history != 0:
                    driver.setParam("DUMP_ALERT_HISTORY", 0)
                    self.dump_alert_history()
                if dump_fault_history != 0:
                    driver.setParam("DUMP_FAULT_HISTORY", 0)
                    self.dump_fault_history()

    def update_pvs(self, driver):
        logging.debug("updating temp PVs")

        for zone in self.zones:
            if len(zone.deriv_data.data) <= 0:
                logging.warning(f"There is no data for zone {zone.name}.  Not reporting temperatures.")
            else:
                driver.setParam(zone.temp_pv, zone.filtered_avg_data[-1])
                driver.setParam(zone.rate_pv, zone.deriv_data.data[-1])
                driver.setParam(zone.quality_pv, zone.filtered_quality[-1])

        self.sort_alarm_groups()

        for alarm_group in self.zone_alarm_groups:
            if alarm_group.last_alarm_s > self.last_zone_alarm_s:
                self.last_zone_alarm_s = alarm_group.last_alarm_s
                driver.setParam("ALERT_GPS_TIME", self.last_zone_alarm_s)
                driver.setParam("ALERT_MESSAGE", alarm_group.alarm_text)
                driver.setParam("ALERT_TIMESTAMP", gps_to_timestamp(self.last_zone_alarm_s))
                break

        if self.fault_alarm_group.last_alarm_s > self.last_fault_alarm_s:
            self.last_fault_alarm_s = self.fault_alarm_group.last_alarm_s
            driver.setParam("FAULT_GPS_TIME", self.last_fault_alarm_s)
            driver.setParam("FAULT_MESSAGE", self.fault_alarm_group.alarm_text)
            driver.setParam("FAULT_TIMESTAMP", gps_to_timestamp(self.last_fault_alarm_s))

        self.update_history(driver)
        self.update_current_alerts(driver)

        self.set_pv_status(driver)

        self.initializing = False

    def update_history(self, driver):
        """
        Update history PVs
        """
        alert_history = merge_alarm_group_histories(self.zone_alarm_groups)
        fault_history = self.fault_alarm_group.alarm_history
        for i in range(NUM_HISTORICAL_CHANNELS):
            if i < len(alert_history):
                driver.setParam(f"ALERT_HISTORY_GPS_TIME_{i}", alert_history[i].gps_time)
                driver.setParam(f"ALERT_HISTORY_TIMESTAMP_{i}", alert_history[i].timestamp)
                driver.setParam(f"ALERT_HISTORY_MESSAGE_{i}", alert_history[i].message)
            if i < len(fault_history):
                driver.setParam(f"FAULT_HISTORY_GPS_TIME_{i}", fault_history[i].gps_time)
                driver.setParam(f"FAULT_HISTORY_TIMESTAMP_{i}", fault_history[i].timestamp)
                driver.setParam(f"FAULT_HISTORY_MESSAGE_{i}", fault_history[i].message)

    def update_current_alerts(self, driver):
        i = 0
        for alarm_group in self.zone_alarm_groups:
            for alarm in alarm_group.set_alarms:
                if i >= NUM_CURRENT_ALARMS:
                    break
                driver.setParam(f"ALERT_GPS_TIME_{i}", alarm.set_time_s)
                driver.setParam(f"ALERT_TIMESTAMP_{i}", alarm.set_timestamp)
                driver.setParam(f"ALERT_MESSAGE_{i}", alarm.text)
                i += 1
        while i < NUM_CURRENT_ALARMS:
            driver.setParam(f"ALERT_GPS_TIME_{i}", 0)
            driver.setParam(f"ALERT_TIMESTAMP_{i}", "")
            driver.setParam(f"ALERT_MESSAGE_{i}", "")
            i += 1

        i = 0

        for alarm in self.fault_alarm_group.set_alarms:
            if i >= NUM_CURRENT_ALARMS:
                break
            driver.setParam(f"FAULT_GPS_TIME_{i}", alarm.set_time_s)
            driver.setParam(f"FAULT_TIMESTAMP_{i}", alarm.set_timestamp)
            driver.setParam(f"FAULT_MESSAGE_{i}", alarm.text)
            i += 1
        while i < NUM_CURRENT_ALARMS:
            driver.setParam(f"FAULT_GPS_TIME_{i}", 0)
            driver.setParam(f"FAULT_TIMESTAMP_{i}", "")
            driver.setParam(f"FAULT_MESSAGE_{i}", "")
            i += 1

    def dump_alerts(self):
        total = 0
        for group in self.zone_alarm_groups:
            total += group.log_set_alarms()
        logging.critical(f"dumped {total} alerts")

    def dump_faults(self):
        total = self.fault_alarm_group.log_set_alarms()
        logging.critical(f"dumped {total} faults")

    def dump_alert_history(self):
        alert_history = merge_alarm_group_histories(self.zone_alarm_groups)[:MAX_ALARM_HISTORY]
        for alarm_record in alert_history:
            logging.critical(f"{alarm_record.timestamp} ({alarm_record.gps_time}) {alarm_record.message}")
        logging.critical(f"dumped {len(alert_history)} entries")

    def dump_fault_history(self):
        fault_history = self.fault_alarm_group.alarm_history
        for alarm_record in fault_history:
            logging.critical(f"{alarm_record.timestamp} ({alarm_record.gps_time}) {alarm_record.message}")
        logging.critical(f"dumped {len(fault_history)} entries")

    def set_pv_status(self, driver):
        """
        Set pv status for all pvs with status controlled by an alarm
        """

        # get all alarms
        alarms = []
        for zone in self.zone_alarm_groups:
            alarms += zone.alarms
        alarms += self.fault_alarm_group.alarms

        new_pv_status = {}

        # find the highest requested alarm level
        for alarm in alarms:
            pv, status = alarm.get_pv_status()
            if pv not in new_pv_status:
                new_pv_status[pv] = pcaspy.Severity.NO_ALARM
            if status > new_pv_status[pv]:
                new_pv_status[pv] = status

        # then set to pv
        for pv, status in new_pv_status.items():
            logging.debug(f"Setting alarm severity of {pv} to {status}")
            driver.setParamStatus(pv, pcaspy.Alarm.HIHI_ALARM, status)


if __name__ == "__main__":
    ts = TemperatureScanner()
    ts.run_test()
