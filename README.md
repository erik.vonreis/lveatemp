# IOC for LVEA temperatures

Monitors temperatures by zone in LVEA at LHO.

Checks for temperature rate of change as well as limits.


# Sensor faults.

A sensor is assumed ot be at fault if data is not available or if the sensor quickly jumps to 0 or below degrees, or 
up to 40.

The IOC raises a separate alarm for sensor fault.  When a sensor is in fault, the sensor's last good value is used and its 'quality' 
estimate gradually decreases.

# EPICS PVs

All EPICS PVs have this prefix: ```H1:CDS-FMCS_STAT_```

## EPICS outputs

The IOC creates these EPICS Process Variables (pvs) as outputs

### General outputs

| PV              | Descriptions                                             |
|-----------------|----------------------------------------------------------|
| GPS_TIME        | Time of last update in GPS seconds                       |
| UPTIME_SECONDS  | Seconds since start of IOC                               |
| TIMESTAMP       | Time of last update as a string                          |
| HOSTNAME        | name of host IOC is running on                           |
| ALERT_GPS_TIME  | Time of last temperature alarm in GPS seconds            |
| ALERT_MESSAGE   | Text of last temperature alarm                           |
| ALERT_TIMESTAMP | Time of last temperature alarm as a string in local time |
| FAULT_GPS_TIME  | Time of last sensor fault alarm in GPS seconds           |
| FAULT_MESSAGE   | Text of last sensor fault alarm |
| FAULT_TIMESTAMP | Time of last sensor fault alarm as a string in local time |

### Zone outputs

These EPICS PVs are created one per temperature zone.
The name is slightly different for each temperature zone.

These PVs all have the zone name included in the PV name.
The zone name replaces {zone}, so that ```ZONE{zone}_DEGC``` becomes 
```ZONE2A_DEGC``` for zone 2A.

| PV | Description |
| ---- | ---- |
| ZONE{zone}_DEGC | Estimated average temperature in the zone |
| ZONE{zone}_DEGC_PER_MIN | Estimated rate of change of temperature for the zone.  This value is heavily filtered and lags behind the temperature value. |
| ZONE{zone}_QUALITY | An estimate of how good the rate of change value is. Quality goes down when sensors are in fault, then comes back slowly.  Maximum quality is 1.  Minimum is 0.|

## EPICS inputs

These PVs control the behavior of the IOC.  They can be changed 
through epics.

The IOC might need as much as 1 minute for a change to take effect.

### Zone inputs

| PV                      | Description                                                           |
|-------------------------|-----------------------------------------------------------------------|
| ZONE{zone}_HIALARM_DEGC | An alarm is raised if temperature for the zone rises above this value |
| ZONE{zone}_LOALARM_DEGC | An alarm is raised if temperature for the zone falls below this value |
| ZONE{zone}_RATEALARM_DEGC_PER_MIN | An alarm is raised if the rate of change of the temperature exceeds this value. |

### Active Alarm outputs

These outputs show up to 10 active alarms.  There are separate variables
for sensor faults.

These channels have a number {i}, which can be from '0' to '9'.  The sorting of which alerts are shown
first is undefined.

e.g. ```ALERT_GPS_TIME_0```

| PV                  | Description |
|---------------------| --- |
| ALERT_GPS_TIME_{i}  | gps time for the 'i'th alert as integer |
| ALERT_TIMESTAMP_{i} | timestamp for the 'i'th alert as string |
| ALERT_MESSAGE_{i}   | message for the 'i'th alert as string|
| FAULT_GPS_TIME_{i}  | gps time for the 'i'th fault as integer |
| FAULT_TIMESTAMP_{i} | timestamp for the 'i'th fault as string |
| FAULT_MESSAGE_{i}   | message for the 'i'th fault as string|

### Alarm history outputs

These output show the last 10 alerts, or the last 10 faults that
were set.

The channels are shown with an {i}, which can take on values '0' through '9'.
The '0' variable is the latest entry, '9' is the earliest.

e.g. ```ALERT_HISTORY_GPS_TIME_0```

| PV | Description                             |
| --- |-----------------------------------------|
| ALERT_HISTORY_GPS_TIME_{i} | gps time for the 'i'th alert as integer |
| ALERT_HISTORY_TIMESTAMP_{i} | timestamp for the 'i'th alert as string |
| ALERT_HISTORY_MESSAGE_{i} | message for the 'i'th alert as string   |
| FAULT_HISTORY_GPS_TIME_{i} | gps time for the 'i'th fault as integer |
| FAULT_HISTORY_TIMESTAMP_{i} | timestamp for the 'i'th fault as string |
| FAULT_HISTORY_MESSAGE_{i} | message for the 'i'th fault as string   |

### Log Dump inputs

These inputs cause the IOC to dump some store info to its output.
These are logged at 'CRITICAL' log level, so that you'd have to make the IOC
very quiet to miss these outputs.

| PV | Description |
| --- | --- |
| DUMP_ALERTS | Write a non-zero value to dump active alerts to the log |
| DUMP_ALERT_HISTORY | Write a non-zero value to dump the last 100 alerts to the log |
| DUMP_FAULTS | Write a non-zero value to dump active faults to the log |
| DUMP_FAULT_HISTORY | Write a non-zero value to dump the last 100 faults to the log |
