#!/usr/bin/env python3

"""
Python pcaspy EPICS IOC to report the overall status of FMCS

List of status items:

LVEA TEMP:
    For each zone, each hour perform a 24 hour average
    for the current period and previous period.
    Raise an alert if the difference in these averages exceeds
    the max diff value. Do no use any sensor which has
    outlier data. If a sensor continually has outliers for
    48 hours or more, raise a failed sensor alert

PEP8 compliant

D.Barker LHO 01sep2022: first version
E. von Reis LHO 17nov2022: measures and alarms on rate, produces history
E. von Reis LHO 23feb2023: protect from NaN inputs

"""

import sys
from datetime import datetime

from gpstime import gpstime
from pcaspy import Driver
from pcaspy import SimpleServer
import platform
import threading
import time
from temp_scanner import TemperatureScanner
import logging
import argparse

TEST = False

if TEST:
    # Testing
    IFO = "H3"
    LOOP_TIME_SECONDS = 10  # scan every 1 second
    LOOP_SLEEP = 1
    UPDATE_ONE_ONLY = True   # advance only one minute at a time
    CHECK_LEADIN = True   # whether to check for alerts on lead in data
    EARLY_LEADIN_S = 0
else:
    # Production
    IFO = "H1"
    LOOP_TIME_SECONDS = 60  # scan every 1 minute
    LOOP_SLEEP = 1  # read pvs every second
    UPDATE_ONE_ONLY = False  # advance to latest current data
    CHECK_LEADIN = False  # whether to check for alerts on lead in data
    EARLY_LEADIN_S = 0

CA_GOOD = 1
CA_ERROR = 0
BAD_VAL = -1.23

prefix = f"{IFO}:CDS-FMCS_STAT_"
pvdb = {
    'GPS_TIME': {
        'type': 'int',
    },
    'UPTIME_SECONDS': {
        'type': 'int',
    },
    'TIMESTAMP': {
        'type': 'string',
    },
    'ERROR_MESSAGE': {
        'type': 'string',
    },
    'HOSTNAME': {
        'type': 'string',
    },
    'ALERT_GPS_TIME': {
        'type': 'int',
    },
    'ALERT_MESSAGE': {
        'type': 'string',
    },
    'ALERT_TIMESTAMP': {
        'type': 'string',
    },
    'FAULT_GPS_TIME': {
        'type': 'int',
    },
    'FAULT_MESSAGE': {
        'type': 'string',
    },
    'FAULT_TIMESTAMP': {
        'type': 'string',
    },
    'DUMP_ALERTS': {
        'type': 'int',
    },
    'DUMP_FAULTS': {
        'type': 'int',
    },
    'DUMP_ALERT_HISTORY': {
        'type': 'int',
    },
    'DUMP_FAULT_HISTORY': {
        'type': 'int',
    },
}


def timestamp_now():
    """Return current time as string

    """
    now = datetime.now()
    timestamp = now.strftime("%H:%M:%S %d%b%Y")
    return timestamp


def gps_now():
    """Return current GPS time

    """
    gps = int(gpstime.tconvert('now').gps())
    return gps


temp_scanner = None


class MyDriver(Driver):
    def __init__(self):
        """Initialize IOC

        Create background thread to run wap_status code
        """
        Driver.__init__(self)
        tid = threading.Thread(target=self.process)
        tid.setDaemon(True)
        tid.start()

    def process(self):
        try:
            """Main processing loop ran in its own thread.
    
            From the IOP model read each ADC and DAC status
            via channel access.
            """
            global temp_scanner
            self.setParam("HOSTNAME", str(platform.node()))
            start_time_s = gps_now()
            # main processing loop
            last_check_gps = 0
            while True:
                gps = gps_now()
                self.setParam("UPTIME_SECONDS",
                              gps - start_time_s)
                temp_scanner.read_pvs(self)
                if gps - last_check_gps >= LOOP_TIME_SECONDS:
                    # update all the timing PVs
                    self.setParam("TIMESTAMP", timestamp_now())
                    self.setParam("GPS_TIME", gps)
                    last_check_gps = gps
                    temp_scanner.tick(UPDATE_ONE_ONLY)
                    temp_scanner.update_pvs(self)

                # update pvs and wait for next loop
                self.updatePVs()
                time.sleep(LOOP_SLEEP)
        except Exception as e:
            logging.critical(f"Unhandled exception in temperature loop: {str(e)}")
            sys.exit(1)


if __name__ == '__main__':
    """ Main program
    Process IOC in 10Hz loop after initializations have completed.
    """
    parser = argparse.ArgumentParser(prog="fmcs_stat", description="IOC for monitoring LVEA temps")
    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('-q', '--quiet', action='count', default=0)
    args = parser.parse_args()

    # set log level
    default_log_level = logging.INFO
    verb_level = args.verbose * 10
    quiet_level = args.quiet * 10
    verb_level -= quiet_level
    if verb_level > default_log_level:
        log_level = 0
    else:
        log_level = default_log_level - verb_level

    logging.basicConfig(level=log_level)

    # log some variables that change during testing
    logging.debug(f"TEST={TEST}, IFO={IFO}, LOOP_TIME_SECONDS={LOOP_TIME_SECONDS}, LOOP_SLEEP={LOOP_SLEEP}")
    logging.debug(f"UPDATE_ONE_ONLY={UPDATE_ONE_ONLY}, EARLY_LEADIN_S={EARLY_LEADIN_S}, CHECK_LEADIN={CHECK_LEADIN}")

    # setup the monitor
    temp_scanner = TemperatureScanner()
    temp_scanner.add_pvs(pvdb)
    temp_scanner.prep(EARLY_LEADIN_S, CHECK_LEADIN)

    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = MyDriver()

    while True:
        # process CA transactions
        server.process(0.1)
