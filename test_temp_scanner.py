import matplotlib.pyplot as plt
import scipy.signal as signal
import numpy as np

from temp_scanner import get_leadin_data, create_filter, deriv_filter, rate_limit_degc_per_min, merge_filters, get_zones, get_more_data, get_test_data, get_test_data2

def generate_test_rise(start_val, stop_val, rate_per_min):


    start = np.array([start_val]*500000)
    rise = np.arange(start_val, stop_val, rate_per_min)
    flat = np.array([stop_val]*100000)

    return np.concatenate([start, rise, flat])

def graph_data():
    zones = get_leadin_data()
    zones[0].sensors[0].process()
    plt.plot(zones[0].sensors[0].raw_mean)
    plt.plot(zones[0].sensors[0].raw_max)
    plt.plot(zones[0].sensors[0].raw_min)
    plt.plot(zones[0].sensors[0].raw_sdev)
    plt.plot(zones[0].sensors[0].data)
    plt.plot(zones[0].sensors[0].quality)
    plt.show()

def graph_filtered_data():
    zones = get_leadin_data()
    zones[0].sensors[0].process()

    filt = create_filter()

    create_graph(zones[0].sensors[0].data, filt)


def create_graph(sig, filt):
    z = signal.lfilter_zi(*filt[:2])
    z *= sig[0]
    fd, zo = signal.lfilter(*filt, x=sig, zi=z)

    dfilt = deriv_filter()

    z_d = signal.lfilter_zi(*dfilt[:2])
    z_d *= fd[0]
    dfd, z_do = signal.lfilter(*dfilt[:2], x=fd, zi=z_d)
    # dfd2 = signal.lfilter(*dfilt[:2], x=zones[0].sensors[0].data)

    plt.plot(sig)
    plt.plot(fd)
    plt.plot(dfd * 100000)

    plt.show()


def graph_med_data():
    zones = get_leadin_data()
    sensor = zones[0].sensors[0]
    sensor.process()

    #fd = signal.medfilt(sensor.data, 1441)
    fd = signal.wiener(sensor.data, 1441)

    plt.plot(sensor.data)
    plt.plot(fd)

    plt.show()


def graph_filtered_test_rise():
    rise = generate_test_rise(20,300,0.01)
    filt = create_filter()
    create_graph(rise, filt)


def filt_test():
    dfilt = create_filter()
    print(dfilt)
    w, f = signal.freqz(dfilt[0], dfilt[1])
    m = np.abs(f*np.conj(f))
    fig = plt.figure()
    p = fig.add_subplot(2,1,1)
    p.set_yscale('log')
    p.plot(w,m)
    plt.show()


def filt_test2():
    t = np.arange(0, 100000)
    s = np.sin(2*np.pi*t/8) + 1
    #s = np.array([1]*100000)
    dfilt = create_filter()
    print(dfilt)
    print(signal.tf2zpk(*dfilt[:2]))

    z = [s[0]] * (len(dfilt[0])-1)


    #z = [1,1]
    #z[1] = dfilt[0][2] - dfilt[1][2]
    #z[0] = dfilt[0][1] - dfilt[1][1] + z[1]
    z = [0, 0]
    print(z)

    fs = signal.lfilter(dfilt[0], dfilt[1], s)
    print(fs[-1])

    print(fs)

    plt.plot(s)
    plt.plot(fs)
    plt.show()


def rate_test():
    rates_deg_per_min = [0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.0011, 0.0007, 0.00061, 0.0005, 0.0002, 0.0001]
    filt = create_filter()
    dfilt = deriv_filter()

    filt = merge_filters(filt, dfilt)

    zi_base = signal.lfilter_zi(*filt[:2])

    for rate_deg_per_min in rates_deg_per_min:
        sig = generate_test_rise(20,30,rate_deg_per_min)

        zi = zi_base * sig[0]
        fd, zo = signal.lfilter(*filt, x=sig, zi=zi)

        found = 0
        for i in range(500000, len(fd)):
            if np.fabs(fd[i]) > rate_limit_degc_per_min:
                found = i
                break
        if found > 0:
            print(f"rise of {rate_deg_per_min} detected in {found - 500000} minutes")
        else:
            print(f"rise of {rate_deg_per_min} was not detected.")


def filt_test3():


    get_test_data2()

    zones = get_zones()

    for zone in zones:
        print(zone.name)
        zone.tick()
        plt.plot(zone.unfiltered_avg_data)
        plt.plot(1000 * zone.deriv_data.data + 20)
        plt.plot(zone.filtered_avg_data)
        # plt.plot(zone.filtered_avg_data)
        # for sensor in zone.sensors:
            # print(f"{zone.name}.{sensor.name}")
            # print(f"n, mean, min, data")
            # for i in range(len(sensor.data)):
            #     print(f"{i}, {sensor.raw_mean[i]}, {sensor.raw_min[i]}, {sensor.data[i]}")
            #
            # break
            # plt.plot(sensor.data)
        # print(len(zone.deriv_data.data), len(sensor.data))
        plt.show()

        # plt.plot(zone.quality)
        # for sensor in zone.sensors:
        #     plt.plot(sensor.quality)
        # plt.show()

    return
    get_more_data()

    for zone in zones:
        print(zone.name)
        zone.tick()

        plt.plot(zone.unfiltered_avg_data)
        for sensor in zone.sensors:
            plt.plot(sensor.data)
        plt.plot(100000 * zone.deriv_data.data)
        plt.show()

        plt.plot(zone.quality)
        for sensor in zone.sensors:
            plt.plot(sensor.quality)
        plt.show()


if __name__ == "__main__":
    #graph_filtered_data()
    #filt_test2()
    # rate_test()
    filt_test3()